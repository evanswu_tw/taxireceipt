package com.twu.refactoring;

public class Taxi implements Transport{

    private static final double PEAK_TIME_MULTIPLIER = 1.2;
    private static final double OFF_PEAK_MULTIPLIER = 1.0;
    private static final int RATE_CHANGE_DISTANCE = 10;
    private static final int PRE_RATE_CHANGE_NON_AC_RATE = 15;
    private static final int POST_RATE_CHANGE_NON_AC_RATE = 12;
    private static final int PRE_RATE_CHANGE_AC_RATE = 20;
    private static final int POST_RATE_CHANGE_AC_RATE = 17;


    private boolean airConditioned;
    private int totalKms;
    private double peakTimeMultiple;
    private double total = 0;


    public Taxi(boolean airConditioned, int totalKms, boolean peakTime) {
        this.airConditioned = airConditioned;
        this.totalKms = totalKms;
        this.peakTimeMultiple = getPeakTimeMultiple(peakTime);
    }


    private double getPeakTimeMultiple(boolean isPeakTime) {
        return isPeakTime ? PEAK_TIME_MULTIPLIER : OFF_PEAK_MULTIPLIER;

    }

    @Override
    public double calculateCosts() {
        if (airConditioned) {
            total += getPreRate(PRE_RATE_CHANGE_AC_RATE);
            total += getPostRate(POST_RATE_CHANGE_AC_RATE);
        } else {
            total += getPreRate(PRE_RATE_CHANGE_NON_AC_RATE);
            total += getPostRate(POST_RATE_CHANGE_NON_AC_RATE);
        }
        return total;
    }

    private double getPreRate(int preRateChangeAcRate) {
        return Math.min(RATE_CHANGE_DISTANCE, totalKms) * preRateChangeAcRate * peakTimeMultiple;
    }

    private double getPostRate(int postRateChangeAcRate) {
        return Math.max(0, totalKms - RATE_CHANGE_DISTANCE) * postRateChangeAcRate * peakTimeMultiple;
    }


}
