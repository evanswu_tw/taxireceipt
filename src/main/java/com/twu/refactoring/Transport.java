package com.twu.refactoring;

public interface Transport {

    double calculateCosts();

}
