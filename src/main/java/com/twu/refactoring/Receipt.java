package com.twu.refactoring;

// when refactoring, we need to think about domain and how to model object. 
// For example, when we think about Receipt, Receipt should be generic enough so that it can be use for other purpose
// therefore in this case we need to make sure we move all the "other stuff" to more specific class
public class Receipt {

    // if a receipt is generic, it should not need to know anything about it
    // on the other hand, all these charges are specific to taxi
    // image if we need to use receipt class for other public transport like bus or train which has different charge structure
    private static final int FIXED_CHARGE = 50;
    private static final double SALES_TAX_RATE = 0.1;
    private Transport transport;


    public Receipt(Transport transport) {
        this.transport = transport;
    }

    public double getTotalCost() {
        double totalCost = 0;
        totalCost += FIXED_CHARGE;
        totalCost += transport.calculateCosts() * (1 + SALES_TAX_RATE);
        return totalCost;
    }


    // all the calculation below are specific to taix.
    // once again image if we need to use receipt for other public
    // transport, then all of tese method will be in the wrong place


}
